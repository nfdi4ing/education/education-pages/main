## Beispielvorlage


## Hinweise

* Navigieren Sie mit den Pfeiltasten oder dem Pfeilsymbolen. Scrollen Sie nach rechts zum nächsten Kapitel und nach unten für die Inhalte des jeweiligen Kapitels. 
<!-- ![Navigation Arrows](media_files/navigation_arrows.png "Navigation Arrows") -->
![Navigation Arrows](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/revealjs/-/raw/master/media_files/navigation_arrows.png)
* Auf den letzten Folien finden Sie Kontakthinweise, Lizenzhinweise und eine Druckfunktion zum Export der Folien als PDF. 
* Erstellen Sie für Feedback ein Issue in unserer Startseite


## Shortcuts Tastatur 

* `[S]`: Speaker-View (Ansicht für Vortragenden) in Pop-Up-Fenster öffnen 
* `[F11]`: Vollbildmodus öffnen bzw. verlassen 
* `[ESC]`: Übersicht aller Folien 
* `[B]` oder `[.]`: Blackout, d.h. Folien zu schwarz blenden (bspw. damit diese nicht ablenken)
* `Pfeiltasten`: Zur vorherigen bzw. nächsten Folie blättern
<!-- * `[ALT]` + `Mausklick` (Windows) bzw. `[STRG]` + `Mausklick` (Linux): An Folie heranzoomen bzw. herauszoomen -->
* `[Pos1]`: Zur ersten Folie (Titelfolie) springen, `[End]`: Zur letzten Folie (Abschlussfolie) springen
